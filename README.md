
![AppIcon](https://gitlab.com/47northlabs/47northlabs/sms-to-slack/raw/develop/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png) 

# SMS to Slack

![minSdkVersion](https://img.shields.io/badge/minSdkVersion-19-yellow.svg?style=true)
![compileSdkVersion](https://img.shields.io/badge/compileSdkVersion-27-green.svg?style=true)

This is an app wich uses a [BroadcastReceiver](https://developer.android.com/reference/android/content/BroadcastReceiver.html) to listen for incoming **SMS** and sending it to a slack webhook (sender and sms content). It is especially used for shared 2 factor authentication.