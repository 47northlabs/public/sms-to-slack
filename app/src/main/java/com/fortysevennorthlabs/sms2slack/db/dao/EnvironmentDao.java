package com.fortysevennorthlabs.sms2slack.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.fortysevennorthlabs.sms2slack.db.models.Environment;

import java.util.List;

@Dao
public interface EnvironmentDao {

    @Query("SELECT * from environments")
    LiveData<List<Environment>> getAllEnvironmentsObserver();

    @Query("SELECT * from environments")
    List<Environment> getAllEnvironments();

    @Query("SELECT name from environments")
    String[] getEnvironmentNames();

    @Query("SELECT * from environments WHERE name = :name")
    Environment getEnvironmentByName(String name);

    @Query("SELECT * from environments WHERE id = :id")
    Environment getEnvironmentById(long id);

    @Query("SELECT * from environments WHERE name = :name OR link = :link")
    Environment getEnvironmentByNameOrLink(String name, String link);

    @Delete
    void delete(Environment environment);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Environment> environments);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Environment environment);
}
