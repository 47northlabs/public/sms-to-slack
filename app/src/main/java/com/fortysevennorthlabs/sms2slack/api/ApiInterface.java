package com.fortysevennorthlabs.sms2slack.api;

import com.fortysevennorthlabs.sms2slack.model.SmsMessageRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST
    Call<ResponseBody> sendSmsToSlack(@Url String url, @Body SmsMessageRequest requestBody);
}
