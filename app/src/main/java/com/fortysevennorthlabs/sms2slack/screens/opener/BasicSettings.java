package com.fortysevennorthlabs.sms2slack.screens.opener;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.fortysevennorthlabs.sms2slack.screens.main.MainActivity;
import com.fortysevennorthlabs.sms2slack.R;
import com.fortysevennorthlabs.sms2slack.db.AppDatabase;
import com.fortysevennorthlabs.sms2slack.db.models.Environment;
import com.fortysevennorthlabs.sms2slack.utils.PrefManager;
import com.fortysevennorthlabs.sms2slack.utils.Utils;
import com.fortysevennorthlabs.sms2slack.utils.Variables;

import java.util.LinkedList;
import java.util.List;

public class BasicSettings extends AppCompatActivity {

    AppDatabase appDatabase;
    List<Environment> environments;
    List<ConstraintLayout> layouts = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_settings);

        appDatabase = AppDatabase.getAppDatabase(this);

        this.environments = appDatabase.environmentDao().getAllEnvironments();

        if (getIntent().getIntExtra("old", 0) == 1) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        layouts.add((ConstraintLayout) findViewById(R.id.include1));
        layouts.add((ConstraintLayout) findViewById(R.id.include2));
        layouts.add((ConstraintLayout) findViewById(R.id.include3));
        layouts.add((ConstraintLayout) findViewById(R.id.include4));
        layouts.add((ConstraintLayout) findViewById(R.id.include5));

        addData (layouts);
    }
    private void addData(List<ConstraintLayout> layouts) {
        for (int i = 0; i < layouts.size(); i++) {
            String[] items = new String[]{getResources().getString(R.string.slack)};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
            //set the spinners adapter to the previously created one.
            ((Spinner) layouts.get(i).findViewById(R.id.spinner)).setAdapter(adapter);
            setTextInputFields(i);
            addListeners(i);
        }
    }

    private void addListeners(int i) {
        ((Spinner) layouts.get(i).findViewById(R.id.spinner)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int index, long id) {
                TextInputLayout nameLayout = (TextInputLayout) layouts.get(i).findViewById(R.id.nameLayout);
                TextInputLayout linkLayout = (TextInputLayout) layouts.get(i).findViewById(R.id.linkLayout);
                if (index == 1) {
                    nameLayout.setHint(getResources().getString(R.string.title));
                    linkLayout.setHint(getResources().getString(R.string.email));
                } else {
                    nameLayout.setHint(getResources().getString(R.string.channel));
                    linkLayout.setHint(getResources().getString(R.string.webhook));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void setTextInputFields(int i) {
        String name = this.environments.size() > i ? this.environments.get(i).getName() : null;
        String link = this.environments.size() > i ? this.environments.get(i).getLink() : null;
        ((TextInputEditText) layouts.get(i).findViewById(R.id.name)).setText(name);
        ((TextInputEditText) layouts.get(i).findViewById(R.id.link)).setText(link);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.save:
                Environment env = BasicSettings.this.environments.stream()
                        .filter(e -> e.getLink() == null
                                || e.getName() == null
                                || e.getLink().trim().length() == 0
                                || e.getName().trim().length() == 0)
                        .findAny()
                        .orElse(null);
                if (env == null) {
                    saveData();
                    startMainActivity();
                }
                break;
            case android.R.id.home:
                this.finish();
                return true;

        }
        return(super.onOptionsItemSelected(item));
    }
    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveData() {
        for (int i = 0; i < layouts.size(); i++) {
            String name = ((TextInputEditText) layouts.get(i).findViewById(R.id.name)).getText().toString();
            String link = ((TextInputEditText) layouts.get(i).findViewById(R.id.link)).getText().toString();
            int type = ((Spinner) layouts.get(i).findViewById(R.id.spinner)).getSelectedItemPosition();
            Environment environment = this.appDatabase.environmentDao().getEnvironmentByNameOrLink(name, link);
            if (!name.trim().equals("") && !link.trim().equals("") && environment == null) {
                environment = new Environment(name, link, Utils.getCurrentUTC(), Utils.getCurrentUTC(), type);
            }
            if (environment != null) {
                this.appDatabase.environmentDao().insert(environment);
            }
        }
    }

}
