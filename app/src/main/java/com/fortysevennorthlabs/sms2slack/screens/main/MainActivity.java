package com.fortysevennorthlabs.sms2slack.screens.main;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.fortysevennorthlabs.sms2slack.R;
import com.fortysevennorthlabs.sms2slack.db.AppDatabase;
import com.fortysevennorthlabs.sms2slack.db.models.Rule;
import com.fortysevennorthlabs.sms2slack.preferences.PreferencesActivity;
import com.fortysevennorthlabs.sms2slack.screens.opener.BasicSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ModifyRuleListener{

    static final int IDM_SETTINGS = 101;
    FloatingActionButton fab;
    AlertDialog newRuleDialog;
    private AppDatabase appDatabase;
    RecyclerView rv;
    Adapter adapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, IDM_SETTINGS, Menu.NONE, R.string.menu_settings);
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        rv = (RecyclerView) findViewById(R.id.listOfRules);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDialog(null, null);
            }
        });
        this.appDatabase = AppDatabase.getAppDatabase(this);
        this.appDatabase.ruleDao().getAllRules().observe(this, new Observer<List<Rule>>() {
            @Override
            public void onChanged(@Nullable List<Rule> rules) {
                addAdapter(rules);
            }
        });
    }

    public void addAdapter(List<Rule> rules) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        adapter = new Adapter(rules, this.appDatabase, this);
        rv.setAdapter(adapter);
    }

    private void startDialog(String channelName, String phoneNumberS) {
        final View view = getLayoutInflater().inflate(R.layout.create_new_rule_layout, null);
        newRuleDialog = new AlertDialog.Builder(this).create();
        newRuleDialog.setTitle(getResources().getString(R.string.newRuleTitle));

        this.appDatabase = AppDatabase.getAppDatabase(this);

        Spinner spinner = (Spinner) view.findViewById(R.id.channels);
        Button addRuleButton = (Button) view.findViewById(R.id.submit);
        TextInputEditText phoneNumber = (TextInputEditText) view.findViewById(R.id.phoneNumber);


        String[] items = this.appDatabase.environmentDao().getEnvironmentNames();

                // new String[]{getResources().getString(R.string.slack), getResources().getString(R.string.email)};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        //set the spinners adapter to the previously created one.
        spinner.setAdapter(adapter);


        if (channelName != null && phoneNumberS != null) {
            newRuleDialog.setTitle(getResources().getString(R.string.updateRuleTitle));
            addRuleButton.setText(getResources().getString(R.string.updateRuleTitle));
            phoneNumber.setText(phoneNumberS);
            int position = new ArrayList<String>(Arrays.asList(items)).indexOf(channelName);
            if (position >= 0) {
                spinner.setSelection(position);
            }
        }

        addRuleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneN = phoneNumber.getText().toString().trim();
                long envId = appDatabase.environmentDao().getEnvironmentByName(spinner.getSelectedItem().toString()).getId();
                Rule rule = appDatabase.ruleDao().getRuleByNumberOrEnvId(phoneN, envId);
                if (phoneNumber.getText().toString().trim().length() > 0) {
                    if (rule == null) {
                        rule = new Rule(phoneN, envId);
                    } else {
                        rule.setEnvironmentId(envId);
                        rule.setNumber(phoneN);
                    }
                    appDatabase.ruleDao().insert(rule);
                    phoneNumber.setText(null);
                    spinner.setSelection(0);
                    newRuleDialog.hide();
                }
            }
        });

        newRuleDialog.setView(view);

        newRuleDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.print(item.getItemId());
        if (item.getItemId() == IDM_SETTINGS) {
            Intent intent = new Intent(this, BasicSettings.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("old", 1);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void modifyRule(String number, String name) {
        startDialog(name, number);
        newRuleDialog.show();
    }

}