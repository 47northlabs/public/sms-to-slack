package com.fortysevennorthlabs.sms2slack.model;

import com.google.gson.annotations.SerializedName;

public class SmsMessageRequest {

    @SerializedName("text")
    private String text;

    public SmsMessageRequest() {
    }

    public SmsMessageRequest(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
