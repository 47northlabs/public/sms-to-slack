package com.fortysevennorthlabs.sms2slack.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {@ForeignKey(
        entity = Environment.class,
        parentColumns = "id",
        childColumns = "environmentId")}, tableName = "rules", indices = {@Index(value = {"environmentId"},
        unique = true), @Index(value = {"number"},
        unique = true)})
public class Rule {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String number;
    private long environmentId;

    public Rule(String number, long environmentId) {
        this.number = number;
        this.environmentId = environmentId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(long environmentId) {
        this.environmentId = environmentId;
    }
}
