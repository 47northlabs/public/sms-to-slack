package com.fortysevennorthlabs.sms2slack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import com.fortysevennorthlabs.sms2slack.api.RetrofitServices;
import com.fortysevennorthlabs.sms2slack.db.AppDatabase;
import com.fortysevennorthlabs.sms2slack.db.models.Environment;
import com.fortysevennorthlabs.sms2slack.db.models.Rule;
import com.fortysevennorthlabs.sms2slack.model.SmsMessageRequest;
import com.fortysevennorthlabs.sms2slack.preferences.PreferencesProvider;
import com.fortysevennorthlabs.sms2slack.utils.Variables;

import retrofit2.Callback;

import okhttp3.ResponseBody;

/**
 * A broadcast receiver who listens for incoming SMS and sends message to slack webhook.
 */
public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "SmsBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            String smsSender = "";
            String smsBody = "";
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                smsBody += smsMessage.getMessageBody();
                smsSender += smsMessage.getOriginatingAddress();
            }
            Log.d(TAG, "SMS detected: From " + smsSender + " With text " + smsBody);
            sendSmsToSlack(context, smsSender, smsBody);
        }
    }

    private void sendSmsToSlack(Context context, String smsSender, String smsContent) {
        SmsMessageRequest smsMessageRequest = new SmsMessageRequest("SMS detected: From " + smsSender + "\n" + smsContent);

        Rule rule = AppDatabase.getAppDatabase(context).ruleDao().getRuleByNumber(smsSender);
        String link = PreferencesProvider.getServiceUrl(context);

        if (rule != null) {
            Environment environment = AppDatabase.getAppDatabase(context).environmentDao().getEnvironmentById(rule.getEnvironmentId());
            link = environment.getLink();
            if (environment.getType() == Variables.SLACK) {
                RetrofitServices.sendSmsToSlack(link, smsMessageRequest, responseBodyCallback);
            }
        }


    }

    private Callback<ResponseBody> responseBodyCallback = new retrofit2.Callback<ResponseBody>() {
        @Override
        public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                Log.d(TAG, "POST request to Slack Success");
            } else {
                Log.e(TAG, "POST request to Slack Failed");
            }
        }

        @Override
        public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            Log.e(TAG, "POST request to Slack Failed error = " + (t != null ? t.getMessage() : "null"));
        }
    };

}
