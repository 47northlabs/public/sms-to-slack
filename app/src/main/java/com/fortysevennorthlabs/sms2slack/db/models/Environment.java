package com.fortysevennorthlabs.sms2slack.db.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.fortysevennorthlabs.sms2slack.utils.Variables;


@Entity(tableName = "environments")
public class Environment {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String link;
    private String created_at;
    private String updated_at;
    private int type;

    public Environment(String name, String link, String created_at, String updated_at, int type) {
        this.name = name;
        this.link = link;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.type = type;
    }

    @Ignore
    public Environment() {
        this.name = "";
        this.link = "";
        this.created_at = "";
        this.updated_at = "";
        this.type = Variables.SLACK;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
