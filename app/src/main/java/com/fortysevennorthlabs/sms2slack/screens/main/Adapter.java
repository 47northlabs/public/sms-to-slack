package com.fortysevennorthlabs.sms2slack.screens.main;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.fortysevennorthlabs.sms2slack.R;

import com.fortysevennorthlabs.sms2slack.db.AppDatabase;
import com.fortysevennorthlabs.sms2slack.db.models.Environment;
import com.fortysevennorthlabs.sms2slack.db.models.Rule;
import com.fortysevennorthlabs.sms2slack.utils.Variables;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Adapter extends RecyclerView.Adapter<Adapter.CustomViewHolder> {

    List<Rule> rules;
    AppDatabase appDatabase;
    Context context;
    ModifyRuleListener listener;
    int[] colors;


    public Adapter(List<Rule> rules, AppDatabase appDatabase, ModifyRuleListener listener) {
        this.rules = rules;
        this.appDatabase = appDatabase;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        if (colors == null) {
            colors = context.getResources().getIntArray(R.array.androidcolors);
        }
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rule_item, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, final int position) {
        // holder.name.requestFocus();
        Rule rule = rules.get(position);
        Environment e = this.appDatabase.environmentDao().getEnvironmentById(rule.getEnvironmentId());
        holder.nameText.setText(rule.getNumber());
        holder.channelText.setText(e.getName());
        holder.editRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.modifyRule(rule.getNumber(), e.getName());
            }
        });
        if (colors.length < position || colors[position] == 0) {
            int randomAndroidColor = colors[new Random().nextInt(colors.length)];
            colors[position] = randomAndroidColor;
        }
        holder.card.setBackgroundColor(colors[position]);
    }

    @Override
    public int getItemCount() {
        return this.rules.size();
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTitle;
        private TextView nameText;
        private TextView channelTitle;
        private TextView channelText;
        private ImageButton editRule;
        private CardView card;

        public CustomViewHolder(View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.nameText);
            nameTitle = (TextView) itemView.findViewById(R.id.nameTitle);
            channelText = (TextView) itemView.findViewById(R.id.channelText);
            channelTitle = (TextView) itemView.findViewById(R.id.channelTitle);
            editRule = (ImageButton) itemView.findViewById(R.id.editRule);
            card = (CardView) itemView.findViewById(R.id.cardItem);
        }
    }
}
