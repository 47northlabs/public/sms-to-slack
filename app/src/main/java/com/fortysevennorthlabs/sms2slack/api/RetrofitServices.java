package com.fortysevennorthlabs.sms2slack.api;

import com.fortysevennorthlabs.sms2slack.model.SmsMessageRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class RetrofitServices {

    public static void sendSmsToSlack(String url, SmsMessageRequest smsMessageRequest, Callback<ResponseBody> callback) {
        ApiInterface apiInterface;
        apiInterface = ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.sendSmsToSlack(url, smsMessageRequest);
        call.enqueue(callback);
    }
}
