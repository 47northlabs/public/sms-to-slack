package com.fortysevennorthlabs.sms2slack.screens.main;

public interface ModifyRuleListener {
    public void modifyRule(String number, String name);
}
