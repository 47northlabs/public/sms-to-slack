package com.fortysevennorthlabs.sms2slack.preferences;

import android.content.Context;
import android.preference.PreferenceManager;

public class PreferencesProvider {

    public static String getServiceUrl(Context context) {
        if (context == null)
            return "";
        return PreferenceManager.getDefaultSharedPreferences(context).getString("pref_slack_webhook_url", "");
    }
}
