package com.fortysevennorthlabs.sms2slack.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.fortysevennorthlabs.sms2slack.db.dao.EnvironmentDao;
import com.fortysevennorthlabs.sms2slack.db.dao.RuleDao;
import com.fortysevennorthlabs.sms2slack.db.models.Environment;
import com.fortysevennorthlabs.sms2slack.db.models.Rule;

@Database(entities = {Environment.class, Rule.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase{

    public abstract EnvironmentDao environmentDao();
    public abstract RuleDao ruleDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "sms-token-db")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }
    public static void destroyInstance() {
        INSTANCE = null;
    }
}
